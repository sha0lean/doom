
function tabsHandler (){

  console.log("la fonction est lancée");

  var component         = $('.onglets'),
      componentNav      = component.find('.tabs-nav'),
      componentNavItems = componentNav.find('.tab'),
      componentContents = component.find('.tabs-content');


    componentNavItems.on('click', function(event){
      var targetID = $(this).data('id');

      event.preventDefault();


      console.log('Je clique sur ' + targetID);

      if ( $('#' + targetID).hasClass('tabs-content--inactive') ) {
        console.log('Mon élément est masqué');

        //Gestion de l'affichage du contenu
        componentContents.addClass('tabs-content--inactive');
        $('#' + targetID).removeClass('tabs-content--inactive');
        
        //Gestion de l'affichage du menu
        componentNavItems.removeClass('tabs-nav-item--active');
        $(this).addClass('tabs-nav-item--active');
      } else {
        console.log ('Mon élement est visible')
      }
  });
};




$(document).ready(function() {

  var tabsComponent = $('.onglets');

  console.log("Le DOM est prêt");
// on vérifie qu'il existe bien pour lancer la fonction (opti)
    if ( tabsComponent.length > 0 ) {
      console.log("Mon composants tabs est bien présent");
      tabsHandler();
    }

    else {
      console.log("Mon composants tabs est bien présent");
    }

});  